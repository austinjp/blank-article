# Blank article

Writing articles in markdown with citations and references, output MS Word docs, PDFs, LaTeX.

Requires:

- pandoc
- pandoc-citeproc
- Perl
- Python 2
- Zotero


Prefers:

- Fonts: Carlito and Caladea.

As per <https://wiki.debian.org/SubstitutingCalibriAndCambriaFonts>

```
sudo apt install fonts-crosextra-carlito fonts-crosextra-caladea
```


Expects:

- Markdown
